﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Statistic
{
    public class PrintStatistic
    {
        public static void Main()
        {
            double[] statisticNumbers = new double[] { 5, 35.54, 98.6, 89, 23.54, 2.4, 98.23 };
            PrintStatistics(statisticNumbers);
        }
        public static void PrintStatistics(double[] statisticNumbers)
        {
            Console.WriteLine(CalcMax(statisticNumbers));
            Console.WriteLine(CalcMin(statisticNumbers));
            Console.WriteLine(CalcAvarage(statisticNumbers));
        }

        public static double CalcMax(double[] statisticNumbers)
        {
            double max = statisticNumbers[0];
            for (int i = 1; i < statisticNumbers.Length; i++)
            {
                if (statisticNumbers[i] > max)
                {
                    max = statisticNumbers[i];
                }
            }
            return max;
        }

        public static double CalcMin(double[] statisticNumbers)
        {
            double min = statisticNumbers[0];
            for (int i = 1; i < statisticNumbers.Length; i++)
            {
                if (statisticNumbers[i] < min)
                {
                    min = statisticNumbers[i];
                }
            }
            return min;
        }

        private static double CalcAvarage(double[] statisticNumbers)
        {
            double sum = 0;
            for (int i = 0; i < statisticNumbers.Length; i++)
            {
                sum += statisticNumbers[i];
            }
            double avarage = sum / statisticNumbers.Length;
            return avarage;
        }
    }
}
