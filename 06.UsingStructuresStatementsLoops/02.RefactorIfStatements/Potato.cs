﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02.RefactorIfStatements
{
    class Potato
    {
        public bool HasBeenPeeled { get; set; }

        public bool IsRotten { get; set; }
    }
}
